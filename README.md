# Introduction

The app follows the code structure below:

```
./                              # root
├── src/
│   ├── assets/                 # App's assets
│   ├── config/                 # General application config options.
│   ├── components/             # Stateless UI components
│   ├── containers/             #
│   │   ├── App/                # Wraps page content and passes
│   │   └── DataProvider/       # App state provider
│   ├── services/
│   │   ├── FilterService/      # Logic for filtering data and options.
│   │   └── TrafficMeister/     # Wraps the Traffic Meister API
│   ├── types/                  # Common types definitions
│   ├── utils/                  # Utillity code
│   ├── index.ts                # Main app file
│   └── index.html
├── tsconfig.json               # Typescript config file
└── package.json                # Project info, scrips, deps, etc...
```

# Development

Notice that it's required `npm version >= 5.2.0` to run this app, otherwise We recommend you to remove all `npx` entries from the script property on `package.json` and install Parcel as a global dependency (`npm i -g parcel-bundler`).

Run the steps bellow in the project root directory.

1. `npm install`
2. `npm run build:dev`

The app will be up and running on [http://localhost:3001]("http://localhost:3001")

To run the unit testing:
`npm test`

You should be able to see the code coverage.

**Optional:**
You can give some stylish looking to your testing output by running:
`npm test | tap-nirvana`

# Deployment

## Netlify

You can deploy this app on [Netlify](https://www.netlify.com/) following the steps below:

1. Log in with your Github/Gitlab account (Considering that you had forked this repository).
2. Click on **New site from git** button.
3. You will have to autheticate your account to give Netlify access to this repo on your account
4. Select the project
5. Fill up **Build Command** input field with `npm run build:prod` and **Public directory** with `dist`
6. Click on **Deploy site**
