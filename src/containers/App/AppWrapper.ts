import styled from "styled-components";

const AppWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;

  .page-container {
    width: 60%;

    @media screen and (max-width: 480px) {
      width: 100%;
      margin: 0 10px;
    }
  }
`;

export default AppWrapper;