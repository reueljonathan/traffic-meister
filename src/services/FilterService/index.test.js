import { describe } from "riteway";
import FilterService, { FilterType } from "./";

const data = [
  {
    id: 1,
    type: "car",
    brand: "Bugatti Veyron",
    colors: ["red", "black"],
    img:
      "https://upload.wikimedia.org/wikipedia/commons/thumb/c/c9/Bugatti_Veyron_16.4_%E2%80%93_Frontansicht_%281%29%2C_5._April_2012%2C_D%C3%BCsseldorf.jpg/520px-Bugatti_Veyron_16.4_%E2%80%93_Frontansicht_%281%29%2C_5._April_2012%2C_D%C3%BCsseldorf.jpg"
  },
  {
    id: 2,
    type: "airplane",
    brand: "Boeing 787 Dreamliner",
    colors: ["red", "white", "black", "green"],
    img:
      "https://upload.wikimedia.org/wikipedia/commons/thumb/1/15/All_Nippon_Airways_Boeing_787-8_Dreamliner_JA801A_OKJ_in_flight.jpg/600px-All_Nippon_Airways_Boeing_787-8_Dreamliner_JA801A_OKJ_in_flight.jpg"
  },
  {
    id: 3,
    type: "train",
    brand: "USRA 0-6-6",
    colors: ["yellow", "white", "black"],
    img:
      "https://upload.wikimedia.org/wikipedia/en/thumb/a/a1/UP_4466_Neil916.JPG/600px-UP_4466_Neil916.JPG"
  },
  {
    id: 4,
    type: "airplane",
    brand: "Canadair North Star",
    colors: ["red", "blue", "yellow", "green"],
    img:
      "https://upload.wikimedia.org/wikipedia/commons/thumb/1/1f/BOAC_C-4_Argonaut_Heathrow_1954.jpg/600px-BOAC_C-4_Argonaut_Heathrow_1954.jpg"
  },
  {
    id: 5,
    type: "airplane",
    brand: "Airbus A400M Atlas",
    colors: ["red", "white"],
    img:
      "https://upload.wikimedia.org/wikipedia/commons/thumb/1/1a/A400M-1969.jpg/600px-A400M-1969.jpg"
  },
  {
    id: 6,
    type: "airplane",
    brand: "Bloch MB.131",
    colors: ["yellow", "blue", "brown"],
    img:
      "https://upload.wikimedia.org/wikipedia/commons/e/e5/Bloch_MB_131_San_Diego_Air_%26_Space_Museum_3.jpg"
  },
  {
    id: 7,
    type: "car",
    brand: "Tesla Model X",
    colors: ["red", "black", "blue", "white", "gray"],
    img: "https://commons.wikimedia.org/wiki/File:TESLA_MODEL_X.jpg"
  }
];

describe("FilterService should filter data by property", async assert => {
  const service = FilterService(data);

  {
    const result = service.filterBy("type", "airplane");

    assert({
      given: "An array of vehicles",
      should: "return all airplanes",
      actual: result.length,
      expected: 4
    });
  }

  {
    const result = service.filterBy("type", "car");
    assert({
      given: "An array of vehicles",
      should: "return all cars",
      actual: result,
      expected: [data[0], data[data.length - 1]]
    });
  }

  {
    const result = service.filterBy("type", "train");
    assert({
      given: "An array of vehicles",
      should: "return all trains",
      actual: result,
      expected: [data[2]]
    });
  }
});

describe("FilterService should return an empty array for an empty entry", async assert => {
  const serviceEmptyData = FilterService([]);

  {
    const result = serviceEmptyData.filterBy("type", "color");
    assert({
      given: "An empty array",
      should: "return an empty array",
      actual: result,
      expected: []
    });
  }

  {
    const result = serviceEmptyData.filterByColor("black");

    assert({
      given: "An empty array",
      should: "return an empty array when filtering it by color",
      actual: result,
      expected: []
    });
  }
});

describe("FilterService filter data by color", async assert => {
  const service = FilterService(data);

  {
    const result = service.filterByColor("black");

    assert({
      given: "An array of vehicles",
      should: "return all vehicles with black color",
      actual: result.length,
      expected: 4
    });
  }
});

describe("FilterService filter options", async assert => {
  const service = FilterService(data);
  const initialFilterOptions = service.filterOptions(FilterType.Default);

  {
    const actual = initialFilterOptions[0];
    const expected = [
      { value: "car", label: "car" },
      { value: "airplane", label: "airplane" },
      { value: "train", label: "train" }
    ];

    assert({
      given: "the default filter type",
      should: "show off all vehicle type options",
      actual,
      expected
    });
  }

  {
    const actual = service.filterOptions(
      FilterType.Vehicle,
      initialFilterOptions
    );
    const expected = [];

    assert({
      given: "the default filter type",
      should: "show off all vehicle type options",
      actual,
      expected
    });
  }
});
