import * as React from "react";
import { render } from "react-dom";
import App from "./containers/App";
import DataProvider from "./containers/DataProvider";

const appRootElem = document.getElementById("app");

render(
  <DataProvider>
    <App />
  </DataProvider>,
  appRootElem
);
