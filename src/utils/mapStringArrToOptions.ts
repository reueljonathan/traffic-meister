import { Options } from "../types";

export default (arr: Array<string>): Options =>
  arr.map((res: string) => ({ value: res, label: res }));
