import { describe } from "riteway";
import mapStringToOptions from "./mapStringArrToOptions";

describe("mapStringArrToOptions()", async assert => {
  {
    const actual = mapStringToOptions(["hello"]);
    const expected = [{ value: "hello", label: "hello" }];
    assert({
      given: "A string",
      should: "return a [ Option{value:string, label: string} ] array",
      actual,
      expected
    });
  }

  {
    const atual = mapStringToOptions(["hello", "world"]);
    const expected = [
      { value: "hello", label: "hello" },
      { value: "world", label: "world" }
    ];
  }
});
