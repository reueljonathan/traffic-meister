import * as React from "react";
import HeaderWrapper from "./HeaderWrapper";

const Header = () => (
  <HeaderWrapper>
    <h3>Traffic Meister</h3>
  </HeaderWrapper>
);

export default Header;
