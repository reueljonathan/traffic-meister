import styled from "styled-components";

const HeaderWrapper = styled.div`
  margin: 20px 0 40px 0;
  h3 {
    font-weight: 400;
  }
`;

export default HeaderWrapper;
