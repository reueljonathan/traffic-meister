import * as React from "react";
import Select from "react-select";
import styled from "styled-components";

const selectStyle = {
  control: style => ({ ...style, height: 60, textTransform: "capitalize" }),
  option: style => ({ ...style, textTransform: "capitalize" })
};

const DropdownWrapper = styled.div`
  width: 30%;

  p {
    font-weight: bold;
    color: #707070;
    margin-bottom: 5px;
  }

  @media screen and (max-width: 480px) {
    width: 100%;
  }
`;

const Dropdown = props => (
  <DropdownWrapper>
    <p>{props.label}</p>
    <Select styles={selectStyle} {...props} />
  </DropdownWrapper>
);

export default Dropdown;
