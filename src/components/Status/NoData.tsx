import * as React from "react";
import MessageWrapper from "./MessageWrapper";

const NoData = () => (
  <MessageWrapper>
    <h4>There is no data to show off</h4>
  </MessageWrapper>
);

export default NoData;
