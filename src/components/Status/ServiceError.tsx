import * as React from "react";
import MessageWrapper from "./MessageWrapper";
import serviceFailureIcon from "../../assets/service_error.svg";

const ServiceError = () => (
  <MessageWrapper>
    <h4>
      Error Fetching Data :(
      <br /> Try Again
    </h4>

    <img src={serviceFailureIcon} />
  </MessageWrapper>
);

export default ServiceError;
