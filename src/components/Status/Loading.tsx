import * as React from "react";
import MessageWrapper from "./MessageWrapper";

const Loading = () => (
    <MessageWrapper>
        <h4>
            Loading...
        </h4>
    </MessageWrapper>
);

export default Loading;