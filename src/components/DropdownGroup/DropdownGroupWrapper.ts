import styled from "styled-components";

const DropdownGroupWrapper = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-evenly;
  align-items: center;
  flex-wrap: wrap;
`;

export default DropdownGroupWrapper;
