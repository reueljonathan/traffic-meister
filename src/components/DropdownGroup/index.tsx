import * as React from "react";
import Dropdown from "../Dropdown";
import { Option } from "../../types";
import DropdownGroupWrapper from "./DropdownGroupWrapper";

interface DropdownGroupProps {
  loading?: boolean;
  filteredOptions: Array<Array<Option>>;
  handleChange: (field: string, op: Option) => void;
}

const noData = () => "No Data";

class DropdownGroup extends React.PureComponent<DropdownGroupProps> {
  render(): React.ReactNode {
    const { filteredOptions, loading } = this.props;
    let options = undefined;
    if (filteredOptions && filteredOptions.length > 0) {
      options = filteredOptions;
    }
    return (
      <DropdownGroupWrapper>
        <Dropdown
          label="Vehicle"
          isDisabled={loading}
          noOptionsMessage={noData}
          onChange={(op: Option) => this.props.handleChange("vehicle", op)}
          options={options ? options[0] : []}
        />

        <Dropdown
          label="Brand"
          margin="0 0 0 20px"
          isDisabled={loading}
          noOptionsMessage={noData}
          onChange={(op: Option) => this.props.handleChange("brand", op)}
          options={options ? options[1] : []}
        />

        <Dropdown
          label="Color"
          margin="0 0 0 20px"
          isDisabled={loading}
          noOptionsMessage={noData}
          onChange={(op: Option) => this.props.handleChange("color", op)}
          options={options ? options[2] : []}
        />
      </DropdownGroupWrapper>
    );
  }
}

export default DropdownGroup;
