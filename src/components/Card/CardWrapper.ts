import styled from "styled-components";

interface CardWrapperProps {
  img: string;
}

const CardWrapper = styled.div`
  width: 100%;
  height: 300px;
  position: relative;
  border-radius: 10px;
  background-image: url(${(props: CardWrapperProps) => props.img});
  background-color: #000;
  background-position: center;
  background-size: contain;
  background-repeat: no-repeat;

  .card-body-container {
    width: 100%;
    padding: 0.5em;
    background: rgba(0, 0, 0, 0.7);
    border-radius: 0 0 10px 10px;
    position: absolute;
    bottom: 0;

    h4 {
      color: white;
      font-weight: 400;
    }
  }
`;

export default CardWrapper;
